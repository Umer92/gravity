package com.wiztech.gravity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiztech.gravity.Interface.RequestCallback;
import com.wiztech.gravity.Service.BackgroundSoundService;
import com.wiztech.gravity.Utils.FontManager;
import com.wiztech.gravity.Utils.PreferenceConnector;
import com.wiztech.gravity.Utils.Util;

import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener, MediaPlayer.OnCompletionListener, RequestCallback {

    private ImageView iv_Header;
    private ImageView Btn_Play;
    private ImageView Bulb;
    private ImageView Lightning;
    private ImageView iv_Message;
    private TextView tv_Counter;
    private TextView tv_Counter_Background;
    private TextView tV_Gravity;
    private Button Btn_Share;
    private Button Btn_Exit;
    private Button Btn_Menu;
    private Button Btn_Rate;
    private Button Btn_CloseDrawer;
    private Button Btn_Setting;
    private ImageButton Btn_Up;
    private ImageButton Btn_Down;
    private Typeface Typeface_SevenDigit;
    private Typeface Typeface_SevenDigitalItalic;
    private Typeface Typeface_GoodTimes;
    private DrawerLayout drawer;
    private RelativeLayout drawerView;
    private int currentCounter = 0;
    private final String TAG = this.getClass().getSimpleName();
    private AnimationDrawable lightningEffect;
    private Animation MessageAnimation;
    public Intent svc, svcCopy;
    public boolean shouldSound = false;
    private final int USER_INTERACTION_DELAY = 10000; //10 seconds
    private boolean shouldClick = true;
    //Sounds
    private MediaPlayer PlayerLimitBreaker;
    private boolean isLimitBreakerStarted = false;
    private MediaPlayer mediaPlayer = null;
    public MediaPlayer mp_LoweringGravity = null;
    private boolean shouldPlayerStart = false;
    //Auto Mode Gravity
    private Handler autoGravityHandler;
    //REGEX
    private final String REGEX = "^([0-9]00)";
    //Thunder counter Check
    private int count = 0;
    private boolean nextAnimate = false;
    RequestCallback requestCallback;
    private Handler thunderHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        startBackgroundMusic();
        bindUIComponents();
        adjustSizes();
        animateMessage();
        bindListeners();
        setTypefaces();
        currentCounter = currentCounter();
        setupTouchListeners();
        autoIncrementGravity();
        playButtonAnimation(currentCounter);

    }

    private void adjustSizes() {

        int myDensity = getResources().getDisplayMetrics().densityDpi;
        if (myDensity < 560 && myDensity >= 470) {
            Btn_Up.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen._80sdp);
            Log.d(TAG, "Condition 1");
        } else if (myDensity >= 420 && myDensity < 560) {
            Btn_Down.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen._88sdp);
            Log.d(TAG, "Condition 2");
        } else if (myDensity == 640) {
            Btn_Up.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen._78sdp);
            Log.d(TAG, "Condition 3");
        }
    }

    private void animateMessage() {
        if (!PreferenceConnector.getIsFirstTime(this)) {
            MessageAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.slide_right_to_left);
            iv_Message.setVisibility(View.VISIBLE);
            iv_Message.startAnimation(MessageAnimation);
            PreferenceConnector.setIsFirstTime(this, true);
        }
    }

    private void setupTouchListeners() {
        Btn_Up.setOnTouchListener(new View.OnTouchListener() {

            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mAction, Util.TOUCH_DELAY);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        break;
                    case MotionEvent.ACTION_BUTTON_RELEASE:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        break;
//                    case MotionEvent.ACTION_MOVE:
//                        if (mHandler == null) return true;
//                        mHandler.removeCallbacks(mAction);
//                        mHandler = null;
//                        break;
                }
                return false;
            }

            final Runnable mAction = new Runnable() {
                @Override
                public void run() {
                    incrementCounter();
                    mHandler.postDelayed(this, Util.TOUCH_DELAY);
                }
            };

        });

        Btn_Down.setOnTouchListener(new View.OnTouchListener() {

            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mAction, Util.TOUCH_DELAY);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        break;
                    case MotionEvent.ACTION_BUTTON_RELEASE:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mAction);
                        mHandler = null;
                        break;
                }
                return false;
            }

            final Runnable mAction = new Runnable() {
                @Override
                public void run() {
                    decrementCounter();
                    mHandler.postDelayed(this, Util.TOUCH_DELAY);
                }
            };


        });
    }

    private void setTypefaces() {

        Typeface_SevenDigit = FontManager.getSevenDigitTypeface(this);
        Typeface_SevenDigitalItalic = FontManager.getSevenDigitItalicTypeface(this);
        Typeface_GoodTimes = FontManager.getGoodTimesTypeface(this);
        //TextViews
        tv_Counter_Background.setTypeface(Typeface_SevenDigit);
        tv_Counter.setTypeface(Typeface_SevenDigit);
        tV_Gravity.setTypeface(Typeface_SevenDigit);

        //Buttons
        Btn_Share.setTypeface(Typeface_SevenDigitalItalic);
        Btn_Menu.setTypeface(Typeface_SevenDigitalItalic);
        Btn_Rate.setTypeface(Typeface_SevenDigitalItalic);
        Btn_Exit.setTypeface(Typeface_SevenDigitalItalic);
        Btn_Setting.setTypeface(Typeface_GoodTimes);
        Btn_CloseDrawer.setTypeface(Typeface_GoodTimes);
    }

    private void bindListeners() {
        //ImageViews
        iv_Header.setOnClickListener(this);
        Bulb.setOnClickListener(this);

        //Buttons
        Btn_Play.setOnClickListener(this);
        Btn_Exit.setOnClickListener(this);
        Btn_Menu.setOnClickListener(this);
        Btn_Share.setOnClickListener(this);
        Btn_Up.setOnClickListener(this);
        Btn_Down.setOnClickListener(this);
        Btn_Setting.setOnClickListener(this);
        Btn_CloseDrawer.setOnClickListener(this);
        Btn_Rate.setOnClickListener(this);
    }

    private void bindUIComponents() {
        //ImageViews
        iv_Header = findViewById(R.id.header);
        Btn_Play = findViewById(R.id.Btn_Play);
        Bulb = findViewById(R.id.Bulb);
        Lightning = findViewById(R.id.lightning);
        iv_Message = findViewById(R.id.iv_Message);

        //TextViews
        tv_Counter_Background = findViewById(R.id.tV_Counter_Background);
        tv_Counter = findViewById(R.id.tV_Counter);
        tV_Gravity = findViewById(R.id.tV_Gravity);

        //Buttons
        Btn_Menu = findViewById(R.id.Btn_Menu);
        Btn_Share = findViewById(R.id.Btn_Share);
        Btn_Rate = findViewById(R.id.Btn_Rate);
        Btn_Exit = findViewById(R.id.Btn_Exit);
        Btn_Up = findViewById(R.id.Btn_Up);
        Btn_Down = findViewById(R.id.Btn_Down);
        Btn_Setting = findViewById(R.id.Btn_CloseDrawer);
        Btn_CloseDrawer = findViewById(R.id.Btn_SettingDrawer);

        //Drawer
        drawerView = findViewById(R.id.drawerView);
        drawer = findViewById(R.id.drawer_layout);
        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        lightningEffect = (AnimationDrawable) Lightning.getBackground();

        //tinting drawables
    }

    @Override
    public void onClick(View view) {

        if (shouldClick) {
            switch (view.getId()) {
                case R.id.header:
                    // do your work
                    break;
                case R.id.Btn_Play:
                    // do your work
                    break;

                case R.id.Bulb:
                    //do your work
                    break;

                case R.id.Btn_Exit:
                    triggerButtonSound();
                    exitApplication();
                    break;

                case R.id.Btn_Menu:
                    triggerButtonSound();
                    drawer.openDrawer(drawerView);
                    iv_Message.clearAnimation();
                    iv_Message.setVisibility(View.INVISIBLE);
                    break;

                case R.id.Btn_Share:
                    triggerButtonSound();
                    shareLinkDialog();
                    break;

                case R.id.Btn_Up:
                    if (shouldSound) ArrowSound();

                    incrementCounter();
                    break;

                case R.id.Btn_Down:
                    if (shouldSound) ArrowSound();

                    decrementCounter();
                    break;

                case R.id.Btn_SettingDrawer:
                    //pause the autoGravity thunderHandler
                    if (shouldSound) invokeMediaPlayer(Util.SOUND_BEEP_1);

                    if (autoGravityHandler != null)
                        autoGravityHandler.removeCallbacksAndMessages(null);

                    SettingDialog settingDialog = new SettingDialog(MainActivity.this);
                    settingDialog.show();
                    break;

                case R.id.Btn_CloseDrawer:
                    if (shouldSound) invokeMediaPlayer(Util.SOUND_BEEP_3);
                    drawer.closeDrawer(drawerView);
                    break;

                case R.id.Btn_Rate:
                    triggerButtonSound();
                    launchMarketplace();
                    break;

                default:
                    break;
            }
        }
    }

    private void triggerButtonSound() {
        if (shouldSound) invokeMediaPlayer(Util.SOUND_BEEP_1);
    }

    private void exitApplication() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void launchMarketplace() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    private void incrementCounter() {
        currentCounter = currentCounter();
        if (currentCounter < Util.LIMIT) {
            currentCounter = currentCounter + 10;
            if (currentCounter == 460) activateHeaderAnimation();
            tv_Counter.setText(currentCounter + "");
        } else {
            drawer.closeDrawer(drawerView);
//            Lightning.setVisibility(View.VISIBLE);
//            if (!lightningEffect.isRunning()) {
//                activeLimitBreakerHeader();
//                lightningEffect.start();
//            }
        }

        soundManager(currentCounter);
        playButtonAnimation(currentCounter);
    }

    private void incrementCounter(int selectedGravity) {
        currentCounter = currentCounter();
        if (currentCounter < Util.LIMIT) {
            currentCounter = currentCounter + selectedGravity;
            if (currentCounter == 460) activateHeaderAnimation();
            tv_Counter.setText(currentCounter + "");
        } else {
            drawer.closeDrawer(drawerView);
//            Lightning.setVisibility(View.VISIBLE);
//            if (!lightningEffect.isRunning()) {
//                activeLimitBreakerHeader();
//                lightningEffect.start();
//            }
        }
        soundManager(currentCounter);
        playButtonAnimation(currentCounter);
        if (!(String.valueOf(currentCounter).matches(REGEX)) && currentCounter != 460 && currentCounter != 990) {
            invokeMediaPlayer(Util.SOUND_INC_AUTOMODE);
        }
    }

    private void decrementCounter() {
        currentCounter = currentCounter();
        if (shouldSound)
            if (!shouldPlayerStart) {
                invokeMediaPlayer(Util.SOUND_LOWERING_GRAV);
                shouldPlayerStart = true;
                final Handler mHandler = new Handler();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        shouldPlayerStart = false;
                        mHandler.removeCallbacks(this);
                    }
                }, 1000);
            }

        if (currentCounter <= Util.LIMIT) {
            lightningEffect.stop();
            disableLimitBreakerHeader();
            Lightning.setVisibility(View.INVISIBLE);
        }
        if (currentCounter > 10) {
            currentCounter = currentCounter - 10;
            if (currentCounter < 460) disableLimitBreakerSound();
            tv_Counter.setText(currentCounter + "");
        } else {
            //do nothing
        }
        playButtonAnimation(currentCounter);
    }

    private int currentCounter() {
        return Integer.parseInt(tv_Counter.getText().toString());
    }

    private void activeLimitBreakerHeader() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            iv_Header.setImageDrawable(getResources().getDrawable(R.drawable.header_active_new, getApplicationContext().getTheme()));
        } else {
            iv_Header.setImageDrawable(getResources().getDrawable(R.drawable.header_active_new));
        }
        shouldClick = false;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                shouldClick = true;
            }
        }, USER_INTERACTION_DELAY);
    }

    private void disableLimitBreakerHeader() {
        //Disable Header Glow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            iv_Header.setImageDrawable(getResources().getDrawable(R.drawable.header_disable_2, getApplicationContext().getTheme()));
        } else {
            iv_Header.setImageDrawable(getResources().getDrawable(R.drawable.header_disable_2));
        }
    }

    private void disableBulb() {
        //Disable Bulb Glow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_head_disable, getApplicationContext().getTheme()));
        } else {
            Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_head_disable));
        }
    }

    private void activateHeaderAnimation() {
        Lightning.setVisibility(View.INVISIBLE);
        // Enable Header Glow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            iv_Header.setImageDrawable(getResources().getDrawable(R.drawable.header_active_new, getApplicationContext().getTheme()));
        } else {
            iv_Header.setImageDrawable(getResources().getDrawable(R.drawable.header_active_new));
        }

        //Enable Bulb Glow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_head_active, getApplicationContext().getTheme()));
        } else {
            Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_head_active));
        }
        //Start Animation
//        lightningEffect.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

//                lightningEffect.stop();
                disableLimitBreakerHeader();
//                Lightning.setVisibility(View.INVISIBLE);
            }
        }, USER_INTERACTION_DELAY);

        //Limit Breaker Sound, Since there are two audios for Limit Breaker that should run one after another and should halt if user belows gravity to 460, hence we made an independent mediaPlayer for this purpose
        invokeLimitBreakerSound();

        //activate Thunder Lightning Effects
        initiateThunder();
    }

    private void invokeLimitBreakerSound() {
        PlayerLimitBreaker = MediaPlayer.create(this, Util.SOUND_LIMIT_BREAKER);
        PlayerLimitBreaker.setOnCompletionListener(new LimitBreakerListener());
        PlayerLimitBreaker.start();
        isLimitBreakerStarted = true;
    }

    private void disableLimitBreakerSound() {
        if (PlayerLimitBreaker != null) {
            PlayerLimitBreaker.release();
        }
        disableBulb();
        isLimitBreakerStarted = false;
        if (thunderHandler != null)
            interruptThunder();
    }

    private void interruptThunder() {
        count = 0;
        nextAnimate = false;
        thunderHandler.removeCallbacksAndMessages(null);
        togglethunderLayout(false);
    }

    private void togglethunderLayout(boolean shouldVisble) {
        if (shouldVisble) {
            findViewById(R.id.lightningEffect_One).setVisibility(View.VISIBLE);
            findViewById(R.id.lightningEffect_Two).setVisibility(View.VISIBLE);
            findViewById(R.id.lightningEffect_Three).setVisibility(View.VISIBLE);
            findViewById(R.id.lightningEffect_Four).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.lightningEffect_One).setVisibility(View.GONE);
            findViewById(R.id.lightningEffect_One).setVisibility(View.GONE);
            findViewById(R.id.lightningEffect_Three).setVisibility(View.GONE);
            findViewById(R.id.lightningEffect_Four).setVisibility(View.GONE);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        resumeBackgroundMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //do your work
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopBackgroundMusic();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopBackgroundMusic();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopBackgroundMusic();
        if (autoGravityHandler != null) {
            this.autoGravityHandler.removeCallbacksAndMessages(null);
        }
    }

    private void startBackgroundMusic() {
        //Old Sound Background
        svc = new Intent(MainActivity.this, BackgroundSoundService.class);
        if (PreferenceConnector.getPrefBackgroundmusic(this)) {
            startService(svc);
        }

        //New Sound Background
//        svcCopy = new Intent(MainActivity.this, BackgroundSoundServiceCopy.class);
//        startService(svcCopy);

        shouldSound = PreferenceConnector.getPrefButtonmusic(this);
    }

    private void stopBackgroundMusic() {
        if (svc != null) {
            stopService(svc);
        }
//        if (svcCopy != null) {
//            stopService(svcCopy);
//        }
        if (PlayerLimitBreaker != null) {
            PlayerLimitBreaker.release();
        }
    }

    private void resumeBackgroundMusic() {
        if (PreferenceConnector.getPrefBackgroundmusic(this)) {
            if (svc != null) {
                startService(svc);
            }
        }
        if (PlayerLimitBreaker != null && isLimitBreakerStarted) {
            PlayerLimitBreaker = MediaPlayer.create(MainActivity.this, Util.SOUND_LIMIT_BREAKER_LOOP);
            PlayerLimitBreaker.setLooping(true);
            PlayerLimitBreaker.start();
        }
//        if (svcCopy != null) {
//            startService(svcCopy);
//        }
    }

    private boolean isAutoGravity() {
        return PreferenceConnector.getPrefAutogravity(this);
    }

    public void autoIncrementGravity() {
        if (isAutoGravity()) {
            final int selectedSeconds = Integer.parseInt(PreferenceConnector.getSeconds(this));
            final int selectedGravity = Integer.parseInt(PreferenceConnector.getGravity(this));
            autoGravityHandler = new Handler();
            autoGravityHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (currentCounter + selectedGravity <= Util.LIMIT) {
                        Log.d(TAG, currentCounter + selectedGravity + "");
                        incrementCounter(selectedGravity);
                        autoGravityHandler.postDelayed(this, selectedSeconds * 1000);
                    } else {
                        autoGravityHandler.postDelayed(this, selectedSeconds * 1000);
                        //autoGravityHandler.removeCallbacks(this);
                    }
                }
            }, selectedSeconds * 1000);

        }
    }

    private void soundManager(int gravityCounter) {
        if (shouldSound)
            switch (gravityCounter) {
                case 100:
                    invokeMediaPlayer(Util.INCREASING_100);
                    break;

                case 200:
                    invokeMediaPlayer(Util.INCREASING_200);
                    break;

                case 300:
                    invokeMediaPlayer(Util.INCREASING_300);

                    break;

                case 400:
                    invokeMediaPlayer(Util.INCREASING_400);

                    break;

                case 500:
                    invokeMediaPlayer(Util.INCREASING_500);

                    break;

                case 600:
                    invokeMediaPlayer(Util.INCREASING_600);

                    break;

                case 700:
                    invokeMediaPlayer(Util.INCREASING_700);

                    break;

                case 800:
                    invokeMediaPlayer(Util.INCREASING_800);

                    break;

                case 900:
                    invokeMediaPlayer(Util.INCREASING_900);

                    break;

                case 990:
                    invokeMediaPlayer(Util.SOUND_MAX_GRAV_REACHED);

                    break;


                default:
                    //do nothing
                    break;


            }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.release();

    }

    @Override
    public void callbackListener(String msg) {

    }

    private class LimitBreakerListener implements MediaPlayer.OnCompletionListener {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.release();
            PlayerLimitBreaker = MediaPlayer.create(MainActivity.this, Util.SOUND_LIMIT_BREAKER_LOOP);
            PlayerLimitBreaker.setLooping(true);
            PlayerLimitBreaker.start();
        }
    }

    public void invokeMediaPlayer(int fileName) {
        mediaPlayer = MediaPlayer.create(this, fileName);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.start();
    }

    private void ArrowSound() {
        invokeMediaPlayer(Util.SOUND_BEEP_2);
    }

    private void playButtonAnimation(int duration) {
        if (duration > 0) {
            duration = Math.abs(duration - Util.LIMIT);
//        float dur = duration / 100;
//        int delay = (int) (500 / dur);
//        Log.d(TAG, "duration" + delay);
            final Animation animation = new AlphaAnimation(1, 0);
            animation.setDuration(duration / 2);
            animation.setInterpolator(new StepInterpolator(0.5f));
            animation.setRepeatCount(Animation.INFINITE);
            animation.setRepeatMode(Animation.REVERSE);
            Log.d(TAG, "repeatCount" + animation.getRepeatCount());
            Btn_Play.clearAnimation();
            Btn_Play.startAnimation(animation);
        }
    }

    private int mod(int x, int y) {
        int result = x % y;
        if (result < 0)
            result += y;
        return result;
    }

    private class StepInterpolator implements Interpolator {

        final float mDutyCycle;

        public StepInterpolator(float dutyCycle) {
            mDutyCycle = dutyCycle;
        }

        @Override
        public float getInterpolation(float input) {
            return input < mDutyCycle ? 0 : 1;
        }

    }

    private void PlayButtonClickCode() {
//        if (Util.checkImageResource(this, Btn_Play, R.drawable.ic_play)) {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            Btn_Play.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_white, getApplicationContext().getTheme()));
//                        } else {
//                            Btn_Play.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_white));
//                        }
//                    } else {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            Btn_Play.setImageDrawable(getResources().getDrawable(R.drawable.ic_play, getApplicationContext().getTheme()));
//                        } else {
//                            Btn_Play.setImageDrawable(getResources().getDrawable(R.drawable.ic_play));
//                        }
//
//                    }

//        if (Util.checkImageResource(this, Bulb, R.drawable.ic_bulb_disable)) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_active, getApplicationContext().getTheme()));
//            } else {
//                Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_active));
//            }
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_disable, getApplicationContext().getTheme()));
//            } else {
//                Bulb.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_disable));
//            }
//
//        }
    }

    private void shareLinkDialog() {
        String urlToShare = "https://stackoverflow.com/questions/7545254"; //GooglePlay Link
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        // intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

        // See if official Facebook app is found
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

        // As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }

        startActivity(intent);
    }

    private void initiateThunder() {
        togglethunderLayout(true);
        showThunderBGEffect(R.id.iv_thunderBG);
        thunderAlpha(true);
    }

    private void startThunder(int which) {
        switch (which) {
            case 1:
                //thunderAlpha(true);
                break;
            case 2:
                //thunderBeta(true);
                break;
            case 3:
                //thunderGamma(true);
                break;
            case 4:
                //thunderDelta(true);
                break;
        }
    }

    private void thunderAlpha(boolean b) {
        final int[] thunderIDs = new int[]{R.id.iv_thunder1, R.id.iv_thunder2, R.id.iv_thunder3};
        thunderHandler = new Handler();

        if (b) {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (count == thunderIDs.length) {
                        count = 0;
                        thunderAlpha(false);
                    } else {
                        showLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        } else {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (nextAnimate) {
                        nextAnimate = false;
                        thunderHandler.removeCallbacks(this);
                        showThunderBGEffect(R.id.iv_thunderBG);
                        thunderBeta(true);
                        return;

                    }
                    if (count == thunderIDs.length) {
                        count = 0;
                        nextAnimate = true;
                        thunderHandler.postDelayed(this, Util.THUNDER_NEXT_DELAY);
                    } else {
                        hideLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        }
    }

    private void thunderBeta(boolean shouldAppear) {
        final int[] thunderIDs = new int[]{R.id.iv_thunder8, R.id.iv_thunder9, R.id.iv_thunder10};
        thunderHandler = new Handler();

        if (shouldAppear) {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (count == thunderIDs.length) {
                        count = 0;
                        thunderBeta(false);
                    } else {
                        showLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        } else {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (nextAnimate) {
                        nextAnimate = false;
                        thunderHandler.removeCallbacks(this);
                        showThunderBGEffect(R.id.iv_thunderBG);
                        thunderGamma(true);
                        return;
                    }
                    if (count == thunderIDs.length) {
                        count = 0;
                        nextAnimate = true;
                        thunderHandler.postDelayed(this, Util.THUNDER_NEXT_DELAY);
                    } else {
                        hideLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        }
    }

    private void thunderGamma(boolean shouldAppear) {
        final int[] thunderIDs = new int[]{R.id.iv_thunder4, R.id.iv_thunder5, R.id.iv_thunder6, R.id.iv_thunder7};
        thunderHandler = new Handler();

        if (shouldAppear) {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (count == thunderIDs.length) {
                        count = 0;
                        thunderGamma(false);
                    } else {
                        showLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        } else {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (nextAnimate) {
                        nextAnimate = false;
                        thunderHandler.removeCallbacks(this);
                        showThunderBGEffect(R.id.iv_thunderBG);
                        thunderDelta(true);
                        Log.d(TAG, "Thunder Delta Started");
                        return;

                    }
                    if (count == thunderIDs.length) {
                        count = 0;
                        nextAnimate = true;
                        thunderHandler.postDelayed(this, Util.THUNDER_NEXT_DELAY);
                    } else {
                        hideLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        }
    }

    private void thunderDelta(boolean shouldAppear) {
        final int[] thunderIDs = new int[]{R.id.iv_thunder11, R.id.iv_thunder12, R.id.iv_thunder13};
        thunderHandler = new Handler();

        if (shouldAppear) {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (count == thunderIDs.length) {
                        count = 0;
                        thunderDelta(false);
                    } else {
                        showLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        } else {
            final Runnable runnable = new Runnable() {
                public void run() {
                    if (nextAnimate) {
                        nextAnimate = false;
                        thunderHandler.removeCallbacks(this);
                        showThunderBGEffect(R.id.iv_thunderBG);
                        thunderAlpha(true);
                        return;

                    }
                    if (count == thunderIDs.length) {
                        count = 0;
                        nextAnimate = true;
                        thunderHandler.postDelayed(this, Util.THUNDER_NEXT_DELAY);
                    } else {
                        hideLightningEffect(thunderIDs[count]);
                        count++;
                        thunderHandler.postDelayed(this, Util.THUNDER_INBETWEEN_DELAYS);
                    }
                }
            };

            // trigger first time
            thunderHandler.post(runnable);
        }
    }

    private void showLightningEffect(int id) {
        findViewById(id).setVisibility(View.VISIBLE);
        findViewById(id).setAlpha(0);
        findViewById(id).animate().alpha(1f).start();
    }

    private void hideLightningEffect(int id) {
        findViewById(id).setVisibility(View.VISIBLE);
        findViewById(id).setAlpha(1);
        findViewById(id).animate().alpha(0).start();
    }

    private void showThunderBGEffect(int thunderID){
        findViewById(thunderID).setVisibility(View.VISIBLE);
        findViewById(thunderID).setAlpha(0);
        findViewById(thunderID).animate().alpha(1f).setDuration(50).start();
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                findViewById(R.id.iv_thunderBG).setVisibility(View.GONE);
            }
        };

        handler.postDelayed(r, Util.THUNDER_BG_DELAY);

    }

    private void developersHelp() {
//        findViewById(R.id.lightningEffect_Two).setVisibility(View.VISIBLE);
//        findViewById(R.id.iv_thunder4).setAlpha(1);
//        findViewById(R.id.iv_thunder5).setAlpha(1);
//        findViewById(R.id.iv_thunder6).setAlpha(1);
//        findViewById(R.id.iv_thunder7).setAlpha(1);

        activateHeaderAnimation();
    }
}
