package com.wiztech.gravity.Service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import com.wiztech.gravity.Utils.Util;

/**
 * Created by asd on 8/10/2017.
 */

public class BackgroundSoundServiceCopy extends Service {
    private static final String TAG = "BackgroundSoundService";
    MediaPlayer player;
    public IBinder onBind(Intent arg0) {
        Log.d("BackgroundSoundService", "bind");
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, Util.SOUND_BACKGROUND);
        player.setLooping(true); // Set looping
        player.setVolume(100,100);

    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return START_NOT_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
        Log.d("BackgroundSoundService", "start");
    }
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        Log.d("BackgroundSoundService", "unbind");
        return null;
    }

    public void onStop() {

    }
    public void onPause() {

    }
    @Override
    public void onDestroy() {
        player.stop();
        player.release();
        Log.d("BackgroundSoundService", "destroy");
    }

    @Override
    public void onLowMemory() {

    }
}