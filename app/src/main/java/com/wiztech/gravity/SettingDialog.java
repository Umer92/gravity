package com.wiztech.gravity;

import android.app.Activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.wiztech.gravity.Utils.FontManager;
import com.wiztech.gravity.Utils.PreferenceConnector;
import com.wiztech.gravity.Utils.Util;

/**
 * Created by asd on 8/9/2017.
 */

public class SettingDialog extends Dialog implements View.OnClickListener {

    private String TAG = "SettingDialog";
    Button tV_ResetDefault;
    Button Btn_Close;
    Context context;
    Button Btn_Background_OFF, Btn_Background_ON, Btn_Whistle_OFF, Btn_Whistle_ON, Btn_Gravity_YES, Btn_Gravity_NO;
    Spinner sp_Gravity, sp_Seconds;
    MainActivity parentActivity;
    LinearLayout settingAutoGravity;
    ArrayAdapter<String> gravityAdapter, secondAdapter;

    public SettingDialog(Activity activity) {
        super(activity, R.style.DialogSlideAnim);
        context = activity;
        parentActivity = (MainActivity) activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        this.setContentView(R.layout.layout_setting);
        // getWindow().setGravity(Gravity.BOTTOM);
        // getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        // View v = getWindow().getDecorView();
        //v.setBackgroundResource(android.R.color.transparent);
        bindUIComponents();
        refreshSettings();
        setTypefaces();
        setListeners();
        setupSpinners();
    }

    private void refreshGravityAndSeconds() {
        sp_Gravity.setSelection(gravityAdapter.getPosition(PreferenceConnector.getGravity(context)), false);
        sp_Seconds.setSelection(secondAdapter.getPosition(PreferenceConnector.getSeconds(context)), false);
    }

    private void refreshSettings() {
        if (PreferenceConnector.getPrefBackgroundmusic(context)) BackgroundMusicON();
        else BackgroundMusicOFF();

        if (PreferenceConnector.getPrefButtonmusic(context)) WhistleON();
        else WhistleOFF();

        if (PreferenceConnector.getPrefAutogravity(context)) AutoGravityYES();
        else AutoGravityNO();
    }

    private void setupSpinners() {
        String[] arrayGravity = context.getResources().getStringArray(R.array.Gravity);
        gravityAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, arrayGravity);
        gravityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Gravity.setAdapter(gravityAdapter);

        String[] arraySeconds = context.getResources().getStringArray(R.array.Seconds);
        secondAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, arraySeconds);
        secondAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Seconds.setAdapter(secondAdapter);

        if (PreferenceConnector.getPrefAutogravity(context))
        {
            refreshGravityAndSeconds();
        } else {
            //Since its a bug on SetOnItemClickedListener that it automatically calls itself for the first time, Doing so wouldn't call setOnItemListener Delegate and hence it won't sound.
            sp_Gravity.setSelection(0, false);
            sp_Seconds.setSelection(0, false);
        }

        sp_Gravity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        sp_Seconds.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setTypefaces() {
        Typeface tf_GoodTimes = FontManager.getGoodTimesTypeface(context);
        Btn_Close.setTypeface(tf_GoodTimes, Typeface.BOLD);
        tV_ResetDefault.setTypeface(tf_GoodTimes, Typeface.BOLD);

        Btn_Background_OFF.setTypeface(tf_GoodTimes);
        Btn_Background_ON.setTypeface(tf_GoodTimes);
        Btn_Whistle_OFF.setTypeface(tf_GoodTimes);
        Btn_Whistle_ON.setTypeface(tf_GoodTimes);
        Btn_Gravity_YES.setTypeface(tf_GoodTimes);
        Btn_Gravity_NO.setTypeface(tf_GoodTimes);

    }

    private void setListeners() {
        tV_ResetDefault.setOnClickListener(this);
        Btn_Close.setOnClickListener(this);
        Btn_Background_OFF.setOnClickListener(this);
        Btn_Background_ON.setOnClickListener(this);
        Btn_Whistle_OFF.setOnClickListener(this);
        Btn_Whistle_ON.setOnClickListener(this);
        Btn_Gravity_YES.setOnClickListener(this);
        Btn_Gravity_NO.setOnClickListener(this);
    }

    private void bindUIComponents() {
        //TextViews
        tV_ResetDefault = findViewById(R.id.tV_ResetDefault);

        //Buttons
        Btn_Close = findViewById(R.id.Btn_CloseSetting);
        Btn_Background_OFF = findViewById(R.id.Btn_Background_OFF);
        Btn_Background_ON = findViewById(R.id.Btn_Background_ON);
        Btn_Whistle_OFF = findViewById(R.id.Btn_Whistle_OFF);
        Btn_Whistle_ON = findViewById(R.id.Btn_Whistle_ON);
        Btn_Gravity_YES = findViewById(R.id.Btn_Gravity_YES);
        Btn_Gravity_NO = findViewById(R.id.Btn_Gravity_NO);

        //Spinners
        sp_Gravity = findViewById(R.id.sp_Gravity);
        sp_Seconds = findViewById(R.id.sp_Seconds);

        //Layouts
        settingAutoGravity = findViewById(R.id.setting_AutoGravity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Btn_CloseSetting:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_3);
                this.dismiss();
                parentActivity.shouldSound = PreferenceConnector.getPrefButtonmusic(context);
                PreferenceConnector.setGravity(context, sp_Gravity.getSelectedItem().toString());
                PreferenceConnector.setSeconds(context, sp_Seconds.getSelectedItem().toString());
                parentActivity.autoIncrementGravity();
                break;

            case R.id.tV_ResetDefault:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_3);
                defaultSettings();
                break;

            case R.id.Btn_Background_OFF:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_1);
                BackgroundMusicOFF();
                parentActivity.stopService(parentActivity.svc);
                PreferenceConnector.setPrefBackgroundmusic(context, false);
                break;

            case R.id.Btn_Background_ON:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_1);
                BackgroundMusicON();
                parentActivity.startService(parentActivity.svc);
                PreferenceConnector.setPrefBackgroundmusic(context, true);
                break;

            case R.id.Btn_Whistle_OFF:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_1);
                WhistleOFF();
                PreferenceConnector.setPrefButtonmusic(context, false);
                break;

            case R.id.Btn_Whistle_ON:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_1);
                WhistleON();
                PreferenceConnector.setPrefButtonmusic(context, true);
                break;

            case R.id.Btn_Gravity_YES:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_1);
                AutoGravityYES();
                PreferenceConnector.setPrefAutogravity(context, true);
                break;

            case R.id.Btn_Gravity_NO:
                parentActivity.invokeMediaPlayer(Util.SOUND_BEEP_1);
                AutoGravityNO();
                PreferenceConnector.setPrefAutogravity(context, false);
                break;

            default:
                break;
        }
    }

    private void defaultSettings() {
        //Turn Background Music ON
        PreferenceConnector.setPrefBackgroundmusic(context, true);
        BackgroundMusicON();
        parentActivity.startService(parentActivity.svc);

        //Turn Feedback Sound ON
        PreferenceConnector.setPrefButtonmusic(context, true);
        WhistleON();

        //Turn Auto Gravity Settings OFF
        PreferenceConnector.setPrefAutogravity(context, false);
        AutoGravityNO();
    }

    private void BackgroundMusicON() {
        Btn_Background_OFF.setBackgroundResource(R.drawable.background_transparent);
        Btn_Background_ON.setBackgroundResource(R.drawable.round_corners_green);

    }

    private void BackgroundMusicOFF() {
        Btn_Background_OFF.setBackgroundResource(R.drawable.round_corners_green);
        Btn_Background_ON.setBackgroundResource(R.drawable.background_transparent);

    }

    private void WhistleON() {
        Btn_Whistle_ON.setBackgroundResource(R.drawable.round_corners_green);
        Btn_Whistle_OFF.setBackgroundResource(R.drawable.background_transparent);
    }

    private void WhistleOFF() {
        Btn_Whistle_ON.setBackgroundResource(R.drawable.background_transparent);
        Btn_Whistle_OFF.setBackgroundResource(R.drawable.round_corners_green);
    }

    private void AutoGravityYES() {
        Btn_Gravity_NO.setBackgroundResource(R.drawable.background_transparent);
        Btn_Gravity_YES.setBackgroundResource(R.drawable.round_corners_green);
        settingAutoGravity.setVisibility(View.VISIBLE);
    }

    private void AutoGravityNO() {
        Btn_Gravity_NO.setBackgroundResource(R.drawable.round_corners_green);
        Btn_Gravity_YES.setBackgroundResource(R.drawable.background_transparent);
        settingAutoGravity.setVisibility(View.INVISIBLE);
    }
}
