package com.wiztech.gravity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.wiztech.gravity.Utils.FontManager;

/**
 * Created by asd on 8/10/2017.
 */

public class ShareDialog extends Dialog implements View.OnClickListener {

    Context context;
    MainActivity parentActivity;
    Button Btn_CloseSetting;

    public ShareDialog(Activity activity) {
        super(activity, R.style.DialogSlideAnim);
        context = activity;
        parentActivity = (MainActivity) activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        this.setContentView(R.layout.layout_share);
        // getWindow().setGravity(Gravity.BOTTOM);
        // getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        // View v = getWindow().getDecorView();
        //v.setBackgroundResource(android.R.color.transparent);
        bindUIComponents();
        setTypefaces();
        setListeners();
    }

    private void setTypefaces() {
        Btn_CloseSetting.setTypeface(FontManager.getGoodTimesTypeface(context));
    }

    private void setListeners() {
        Btn_CloseSetting.setOnClickListener(this);
    }

    private void bindUIComponents() {
        //Button
        Btn_CloseSetting = findViewById(R.id.Btn_CloseSetting);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Btn_CloseSetting:
                this.dismiss();
                break;

            default:
                break;
        }
    }
}
