package com.wiztech.gravity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wiztech.gravity.Utils.FontManager;
import com.wiztech.gravity.Utils.PreferenceConnector;
import com.wiztech.gravity.Utils.Util;

/**
 * Created by asd on 8/7/2017.
 */

public class SplashActivity extends Activity {

    TextView tv_Splash;
    ProgressBar progressBar;
    MediaPlayer mediaPlayer;
    int i=0;
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 6500;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        bindUIComponents();
        showProgress();
        invokeBackgroundSound();
        requestHandler();
    }

    private void invokeBackgroundSound() {
        mediaPlayer = MediaPlayer.create(this, Util.SOUND_INTRO);
        mediaPlayer.setOnCompletionListener(new musicCompletionListener());
        mediaPlayer.start();
        initiateSettings();
    }

    private void requestHandler() {
         /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
                //progressBar.clearAnimation();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void animateLoadBar() {
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(100);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(2);
        animation.setRepeatMode(Animation.REVERSE);
        progressBar.startAnimation(animation);
    }

    private void bindUIComponents() {
        tv_Splash = findViewById(R.id.tv_splash);
        progressBar = findViewById(R.id.progressBar);

        tv_Splash.setTypeface(FontManager.getGoodTimesTypeface(this), Typeface.BOLD);

    }

    private void showProgress(){
        CountDownTimer mCountDownTimer;

        progressBar.setProgress(i);
        mCountDownTimer=new CountDownTimer(6000,60) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress"+ millisUntilFinished);
                i++;
                progressBar.setProgress(i);

            }

            @Override
            public void onFinish() {
                //Do what you want
                i++;
                progressBar.setProgress(100);
                animateLoadBar();
            }
        };
        mCountDownTimer.start();
    }

    private class musicCompletionListener implements MediaPlayer.OnCompletionListener {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.release();
        }
    }

    private void initiateSettings(){
        if (!PreferenceConnector.getIsFirstTime(this)){
            PreferenceConnector.setPrefBackgroundmusic(this, true);
            PreferenceConnector.setPrefButtonmusic(this, true);
        }
    }
}
