package com.wiztech.gravity.Utils;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.wiztech.gravity.R;

/**
 * Created by asd on 8/9/2017.
 */

public class ExtraActivity extends Activity {
    RelativeLayout leftRL;
    ImageButton ButtonUp, ButtonDown, ButtonSetting, ButtonClose;
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//      I'm removing the ActionBar.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_extra);
        bindUI();
        adjustSizes();
        leftRL = (RelativeLayout)findViewById(R.id.drawerView);

    }

    private void bindUI() {
        ButtonUp = findViewById(R.id.Btn_Up);
        ButtonDown = findViewById(R.id.Btn_Down);
    }

    private void adjustSizes() {
        int height = getResources().getDimensionPixelSize(R.dimen._80sdp);
        int myDensity = getResources().getDisplayMetrics().densityDpi;
        if (myDensity < 560){
            ButtonUp.getLayoutParams().height = height;
        }
    }

}
