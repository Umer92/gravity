package com.wiztech.gravity.Utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.util.Locale;

/**
 * Created by asd on 8/7/2017.
 */

public class FontManager {

    private static Typeface SevenDigitTypeface;
    private static Typeface GoodTimesTypeface;
    private static Typeface SevenDigitItalicTypeface;

    public static Typeface getSevenDigitTypeface(Context context){

        if (SevenDigitTypeface==null){
            AssetManager am = context.getApplicationContext().getAssets();
            SevenDigitTypeface = Typeface.createFromAsset(am,
                    String.format(Locale.US, "fonts/%s", "Digital-7edited.ttf"));
            return SevenDigitTypeface;
        } else {
            return SevenDigitTypeface;
        }
    }

    public static Typeface getSevenDigitItalicTypeface(Context context){

        if (SevenDigitItalicTypeface==null){
            AssetManager am = context.getApplicationContext().getAssets();
            SevenDigitItalicTypeface = Typeface.createFromAsset(am,
                    String.format(Locale.US, "fonts/%s", "digital-7(italic).ttf"));
            return SevenDigitItalicTypeface;
        } else {
            return SevenDigitItalicTypeface;
        }
    }

    public static Typeface getGoodTimesTypeface(Context context){

        if (GoodTimesTypeface==null){
            AssetManager am = context.getApplicationContext().getAssets();
            GoodTimesTypeface = Typeface.createFromAsset(am,
                    String.format(Locale.US, "fonts/%s", "goodtimesrg.ttf"));
            return GoodTimesTypeface;
        } else {
            return GoodTimesTypeface;
        }
    }
}
