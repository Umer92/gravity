package com.wiztech.gravity.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by asd on 8/10/2017.
 */

public class PreferenceConnector {

    private static final String PREF_BACKGROUNDMUSIC = "BackgroundMusic";
    private static final String PREF_BUTTONMUSIC = "ButtonMusic";
    private static final String PREF_AUTOGRAVITY = "AutoGravity";
    private static final String PREF_GRAVITY = "Gravity";
    private static final String PREF_SECONDS = "Seconds";
    private static final String isFirstTime = "Application";

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences("RkeysPref", Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static int readInteger(Context context, String key) {
        return getPreferences(context).getInt(key, -1);
    }

    private static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    private static String readString(Context context, String key) {
        return getPreferences(context).getString(key, "");
    }

    public static void RemoveAll(Context context) {
        // getEditor(context).clear().commit();
        // getEditor(context).remove(Constant.TT_TOPUP).commit();
        // getEditor(context).remove(Constant.TT_PAY).commit();
    }

    public static void RemoveItem(Context context, String key) {
        getEditor(context).remove(key).commit();
    }

    public static void writeBoolean(Context context, String key, Boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static Boolean readBoolean(Context context, String key) {
        return getPreferences(context).getBoolean(key, false);
    }

    public static Boolean getPrefBackgroundmusic(Context context) {

        return readBoolean(context, PREF_BACKGROUNDMUSIC);
    }

    public static void setPrefBackgroundmusic(Context context, Boolean value) {

        writeBoolean(context, PREF_BACKGROUNDMUSIC, value);
    }

    public static Boolean getPrefButtonmusic (Context context) {

        return readBoolean(context, PREF_BUTTONMUSIC);
    }

    public static void setPrefButtonmusic(Context context, Boolean value) {

        writeBoolean(context, PREF_BUTTONMUSIC, value);
    }

    public static Boolean getPrefAutogravity (Context context) {

        return readBoolean(context, PREF_AUTOGRAVITY);
    }

    public static void setPrefAutogravity(Context context, Boolean value) {

        writeBoolean(context, PREF_AUTOGRAVITY, value);
    }

    public static String getGravity (Context context) {

        return readString(context, PREF_GRAVITY);
    }

    public static void setGravity(Context context, String gravity) {

        writeString(context, PREF_GRAVITY, gravity);
    }

    public static String getSeconds (Context context) {

        return readString(context, PREF_SECONDS);
    }

    public static void setSeconds(Context context, String gravity) {

        writeString(context, PREF_SECONDS, gravity);
    }

    public static void setIsFirstTime(Context context, Boolean value){

        writeBoolean(context,isFirstTime, value );
    }

    public static Boolean getIsFirstTime (Context context){

        return readBoolean(context, isFirstTime);
    }

}