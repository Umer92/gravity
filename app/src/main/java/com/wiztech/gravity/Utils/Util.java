package com.wiztech.gravity.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

import com.wiztech.gravity.R;

/**
 * Created by asd on 8/8/2017.
 */

public class Util {

    public static final int LIMIT = 990;
    public static final int TOUCH_DELAY = 300;
    public static final int INCREASING_100 = R.raw.increasing100times;
    public static final int INCREASING_200 = R.raw.increasing200times;
    public static final int INCREASING_300 = R.raw.increasing300times;
    public static final int INCREASING_400 = R.raw.increasing400times;
    public static final int INCREASING_500 = R.raw.increasing500times;
    public static final int INCREASING_600 = R.raw.increasing600times;
    public static final int INCREASING_700 = R.raw.increasing700times;
    public static final int INCREASING_800 = R.raw.increasing800times;
    public static final int INCREASING_900 = R.raw.increasing900times;
    public static final int SOUND_LIMIT_BREAKER = R.raw.limitbreaker450intro_new;
    public static final int SOUND_LIMIT_BREAKER_LOOP = R.raw.limitbreaker450loop_new;
    public static final int SOUND_MAX_GRAV_REACHED = R.raw.maximumgravityreached;
    public static final int SOUND_SHUTDOWN_GRAV = R.raw.shuttingdowngravitysimulation;
    public static final int SOUND_LOWERING_GRAV = R.raw.loweringgravity;
    public static final int SOUND_GRAV_SIMULATION = R.raw.gravitysimulationactivated;
    public static final int SOUND_DO_MORE = R.raw.youcandomore;
    public static final int SOUND_BEEP_1 = R.raw.beep1;
    public static final int SOUND_BEEP_2 = R.raw.beep2;
    public static final int SOUND_BEEP_3 = R.raw.beep3;
    public static final int SOUND_INTRO = R.raw.intro;
    public static final int SOUND_BACKGROUND = R.raw.soundloop_new2;
    public static final int SOUND_INC_AUTOMODE = R.raw.increasinggravitysim;
    public static final int THUNDER_INBETWEEN_DELAYS = 50;
    public static final int THUNDER_NEXT_DELAY = 3500;
    public static final int THUNDER_BG_DELAY = 50;



    public static boolean checkImageResource(Context ctx, ImageView imageView,
                                             int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }
}
